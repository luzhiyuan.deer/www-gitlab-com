---
layout: markdown_page
title: "Category Direction - Utilization - Consumables Cost Management"
description: "The Utilization strategy page belongs to the Utilization group of the Fulfillment stage"
---

- TOC
{:toc}


## Fulfillment: Utilization - Consumables Cost Management Overview

The Utilization group aims to ensure customers have access to consumables usage data (storage, compute minutes, and transfer units) so that they can make the optimal decisions for their business needs.